package spring.xmlBeanDefinitions;

import spring.xmlBeanDefinitions.testClasses.Order;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InnerBeanAndReferencesForBeanTest {
    @Test
    public void testOrderBeanWithRef() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/innerBeanTest.xml");

        Order orderBeanWithRef = (Order) context.getBean("orderBeanWithRef");
        System.out.println(orderBeanWithRef.toString());
    }

    @Test
    public void testOrderBeanWithInnerBean() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/innerBeanTest.xml");

        Order orderBeanWithInnerBean = (Order) context.getBean("orderBeanWithInnerBean");
        System.out.println(orderBeanWithInnerBean.toString());
    }

    @Test
    public void testOrderBeanWithInnerBeanInCostructor() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/innerBeanTest.xml");

        Order orderBeanWithInnerBeanInCostructor = (Order) context.getBean("orderBeanWithInnerBeanInCostructor");
        System.out.println(orderBeanWithInnerBeanInCostructor.toString());


    }
}
