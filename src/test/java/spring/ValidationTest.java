package spring;

import spring.validation.Customer;
import spring.validation.ValidationService;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.validation.BindingResult;

import java.util.Date;

public class ValidationTest {
    @Test
    public void testValidation() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("validation/validationContext.xml");
        ValidationService validationService = applicationContext.getBean(ValidationService.class);

        Customer customer = createCustomer();
        BindingResult result = validationService.checkCustomer(customer);
        System.out.println(result);
    }

    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setBirthday(new Date());
        customer.setEmail("user@example.com");
        customer.setAge(17);

        return customer;
    }
}
